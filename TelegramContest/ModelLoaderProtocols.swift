//
//  ModelLoader.swift
//  TelegramContes
//
//  Created by Sergey Kovbasyuk on 10/03/2019.
//  Copyright © 2019 Sergey Kovbasyuk. All rights reserved.
//

import Foundation

protocol ModelLoaderInput {
    func getModels()
}

protocol ModelLoaderOutput {
    func didLoadModels(array:Array<DayInfo>)
}
