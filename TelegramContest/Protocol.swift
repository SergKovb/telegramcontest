//
//  Protocol.swift
//  TelegramContes
//
//  Created by Sergey Kovbasyuk on 10/03/2019.
//  Copyright © 2019 Sergey Kovbasyuk. All rights reserved.
//

import UIKit

protocol DayInfoToPoint {
    func point(for dayInfo: DayInfo) -> CGPoint
}
