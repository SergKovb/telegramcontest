//
//  model.swift
//  TelegramContes
//
//  Created by Sergey Kovbasyuk on 09/03/2019.
//  Copyright © 2019 Sergey Kovbasyuk. All rights reserved.
//

import Foundation

class DayInfo:CustomStringConvertible {
    let value: Int
    let date: Date
    
    init(value: Int, date: Date){
        self.value = value
        self.date = date
    }
    
    var description: String {
        return "val:\(value) for Date\(date)"
    }
    
}


