//
//  File.swift
//  ResizeRectangle
//
//  Created by Sergey Kovbasyuk on 10/03/2019.
//  Copyright © 2019 Peter Pohlmann. All rights reserved.
//

import Foundation

struct DayInterval {
    var startDate:Date
    var endDate:Date
}

protocol ViewWithResizeRectDelegate: class {
    func didStartSelect(dayInterval:DayInterval)
    func didFinishSelect(dayInterval:DayInterval)
}
