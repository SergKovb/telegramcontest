//
//  ViewWithResizeRect.swift
//  ResizeRectangle
//
//  Created by Sergey Kovbasyuk on 10/03/2019.
//  Copyright © 2019 Peter Pohlmann. All rights reserved.
//

import UIKit

class ViewWithResizeRect: UIView {
    
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    @IBOutlet weak var resizeRect: UIView!
    var delegate: ViewWithResizeRectDelegate?

    var touchStart = CGPoint.zero
    var proxyFactor = CGFloat(10)
    var resizeTouch = ResizeTouch()
    
    struct ResizeTouch{
        var leftTouch = false
        var rightTouch = false
        var middelTouch = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first{
            
            let touchStart = touch.location(in: self)
            
            resizeTouch.leftTouch = false
            resizeTouch.rightTouch = false
            resizeTouch.middelTouch = false
            
            if  touchStart.x > resizeRect.frame.minX + (proxyFactor*2) &&  touchStart.x < resizeRect.frame.maxX - (proxyFactor*2){
                resizeTouch.middelTouch = true
                return
            }
            
            if touchStart.x > resizeRect.frame.maxX - proxyFactor && touchStart.x < resizeRect.frame.maxX + proxyFactor {
                resizeTouch.rightTouch = true
            }
            
            if touchStart.x > resizeRect.frame.minX - proxyFactor &&  touchStart.x < resizeRect.frame.minX + proxyFactor {
                resizeTouch.leftTouch = true
            }
            
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first{
            let currentTouchPoint = touch.location(in: self)
            let previousTouchPoint = touch.previousLocation(in: self)
            
            var deltaX = currentTouchPoint.x - previousTouchPoint.x
            let maxDelta = rightConstraint.constant
            let minDelta = 0 - leftConstraint.constant
            
            let screenWidth = self.bounds.size.width
            
            
            if resizeTouch.middelTouch{
                deltaX = max(deltaX,minDelta)
                deltaX = min(deltaX,maxDelta)
                leftConstraint.constant += deltaX
                rightConstraint.constant -= deltaX
            }
            
            if resizeTouch.leftTouch {
                leftConstraint.constant = min(leftConstraint.constant + deltaX, screenWidth - rightConstraint.constant - 80 )
            }
            if resizeTouch.rightTouch {
                rightConstraint.constant = min(rightConstraint.constant - deltaX, screenWidth - leftConstraint.constant - 80 )
            }
            
            //start select
            UIView.animate(withDuration: 0.1, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.layoutIfNeeded()
            }, completion: { (ended) in
                //finish select
            })
        }
    }
}
