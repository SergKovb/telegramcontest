//
//  ModelLoader.swift
//  TelegramContes
//
//  Created by Sergey Kovbasyuk on 10/03/2019.
//  Copyright © 2019 Sergey Kovbasyuk. All rights reserved.
//

import Foundation

class ModelLoader:ModelLoaderInput {
    var delegate:ModelLoaderOutput
    
    init(delegate: ModelLoaderOutput){
        self.delegate = delegate
    }
    
    func getModels() {
        var modelsArray:[DayInfo] = []
        let daySeconds = 24 * 60 * 60
        let startDateInterval = 0 - 40 * daySeconds
        let startDate = Date.init(timeIntervalSinceNow: TimeInterval(startDateInterval))
        for i in 0...40 {
            let value = Int.random(in: 0 ... 100)
            let date = Date.init(timeInterval: TimeInterval(0 - daySeconds * i), since: startDate)
            let dayInfo = DayInfo.init(value: value, date: date)
            modelsArray.append(dayInfo)
        }
        delegate.didLoadModels(array: modelsArray)
    }
}
