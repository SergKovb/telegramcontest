//
//  ViewController.swift
//  TelegramContes
//
//  Created by Sergey Kovbasyuk on 09/03/2019.
//  Copyright © 2019 Sergey Kovbasyuk. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ModelLoaderOutput {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let modelLoader = ModelLoader.init(delegate: self)
        modelLoader.getModels()
//        let dayInfo = DayInfo.init(value: 5, date: Date.init(timeInterval: <#T##TimeInterval#>, since: <#T##Date#>))
        // Do any additional setup after loading the view, typically from a nib.
    }

    func didLoadModels(array: Array<DayInfo>) {
        print(array)
    }
}

